package com.example.eventhubskafkaconsumer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [KafkaAutoConfiguration::class])
class EventHubsKafkaConsumerApplication

fun main(args: Array<String>) {
    runApplication<EventHubsKafkaConsumerApplication>(*args)
}
